# Bash-Tac-Toe

A simple implementation of the timeless classic - tic-tac-toe in bash.
The user can play against a NPC or against a different player.

The project was created for educational purposes, to practice bash scripting.
The implemented NPC decision making is naive as it was not the goal of the project.

<div align="center">
![alt text](images/btt_logo.png "Bash-Tac-Toe logo")
</div>

# How to use
You can issue the following commands for a quick start:
```
git clone https://gitlab.com/ArturGulik/bash-tac-toe.git
cd bash-tac-toe
./bash-tac-toe
```
For more information on the script try `./bash-tac-toe --help`
# Screenshots
### Screenshot of the Player vs Computer mode
<div align="center">
![alt text](images/screenshot_1.png "Screenshot of the Player vs Computer mode")
</div>

### Screenshot of the Player vs Player mode
<div align="center">
![alt text](images/screenshot_2.png "Screenshot of the Player vs Player mode")
</div>

# Licensing
Bash-Tac-Toe is released under the [GNU GPLv3](LICENSE).
