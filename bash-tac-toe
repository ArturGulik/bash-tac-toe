#!/usr/bin/env bash

##############################

# Bash-Tac-Toe
#     Copyright (C) 2022  Artur Gulik

#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.

#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.

#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

##############################

MAJOR=1
MINOR=0
VERSION=$MAJOR.$MINOR
YEARS=2022

function copyright(){
	echo "Bash-Tac-Toe" "$VERSION"
	echo "Copyright (C) $YEARS Artur Gulik."
	echo "License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>"
	echo "This is free software: you are free to change and redistribute it."
	echo "There is NO WARRANTY, to the extent permitted by law."
}

# Setting the default values
display_mode=0
p1_symbol="X"
p2_symbol="O"
order=0
count=0

while [[ $# > 0 ]] ; do
	case $1 in
	-h|--help)
		echo -e "Usage:\r\n  bash-tac-toe [options]\n"
		echo -e "Options:"
		echo -e "  -h, --help\t\t\tDisplay this help information and exit"
		echo -e "  -v, --version\t\t\tDisplay version information and exit"
		echo -e "  -d, --display=MODE\t\tSpecify display mode. 0 for classic, 1 for modern. 0 by default"
		echo -e "  -f, --first=SYMBOL\t\tSpecify the symbol for Player One. 'X' by default"
		echo -e "  -s, --second=SYMBOL\t\tSpecify the symbol for Player Two (NPC). 'O' by default"
		echo -e "  -o, --order=ORDER\t\tSpecify the move order. 0 for Player first, 1 for Computer first. 0 by default"
		echo -e "  -c, --count=COUNT\t\tSpecify whether turn count shall be displayed after game end. 0 for no, 1 for yes. 0 by default"
		exit 0
	;;
	-v|--version)
		copyright
		exit 0
	;;
	-d|--display)
		shift
		if [[ $1 == 0 || $1 == 1 ]]; then
			display_mode=$1
			shift
		else
			echo "bash-tac-toe: Unknown display specified: $1. Must be 0 or 1"
			exit 0
		fi
	;;
	-f|--first)
		shift
		p1_symbol=$1
		p1_symbol=${p1_symbol::1}
		shift
	;;
	-s|--second)
		shift
		p2_symbol=$1
		p2_symbol=${p2_symbol::1}
		shift
	;;
	-o|--order)
		shift
		if [[ $1 == 0 || $1 == 1 ]]; then
			order=$1
			shift
		else
			echo "bash-tac-toe: Unknown order specified: $1. Must be 0 or 1"
			exit 0
		fi
	;;
	-c|--count)
		shift
		if [[ $1 == 0 || $1 == 1 ]]; then
			count=$1
			shift
		else
			echo "bash-tac-toe: Unknown count argument: $1. Must be 0 or 1"
			exit 0
		fi
	;;
	--)
		break;
	;;
	*)
		echo "bash-tac-toe: Unknown option $1"
		exit 1
		break
	;;
	esac
done

function display(){
	local m=("$@")
	if [[ ${m[9]} == 0 ]]; then
		echo "    │   │"
		echo "  ${m[0]} │ ${m[1]} │ ${m[2]}"
		echo "────┼───┼────"
		echo "  ${m[3]} │ ${m[4]} │ ${m[5]}"
		echo "────┼───┼────"
		echo "  ${m[6]} │ ${m[7]} │ ${m[8]}"
		echo "    │   │"
	else
		echo "┌───┬───┬───┐"
		echo "│ ${m[0]} │ ${m[1]} │ ${m[2]} │"
		echo "├───┼───┼───┤"
		echo "│ ${m[3]} │ ${m[4]} │ ${m[5]} │"
		echo "├───┼───┼───┤"
		echo "│ ${m[6]} │ ${m[7]} │ ${m[8]} │"
		echo "└───┴───┴───┘"
	fi
}
function npcx(){
	local m=("$@")
	if [[ ${m[1]} == " " || ${m[4]} == " " || ${m[7]} == " " ]]; then
		echo 2
		exit 0
	fi
	if [[ ${m[0]} == " " || ${m[3]} == " " || ${m[6]} == " " ]]; then
		echo 1
		exit 0
	fi
	if [[ ${m[2]} == " " || ${m[5]} == " " || ${m[8]} == " " ]]; then
		echo 3
		exit 0
	fi
	exit 1
}
function npcy(){
	local m=("$@")
	if [[ ${m[(( ${m[9]} - 1 + 3 ))]} == " " ]]; then
		echo 2
		exit 0
	fi
	if [[ ${m[(( ${m[9]} - 1 + 0 ))]} == " " ]]; then
		echo 1
		exit 0
	fi
	if [[ ${m[(( ${m[9]} - 1 + 6 ))]} == " " ]]; then
		echo 3
		exit 0
	fi
	exit 1
}
function check_board(){
	local m=("$@")
	for y in 0 3 6; do
		if [[ ${m[(( $y ))]} == "$p1_symbol" && ${m[(( 1 + $y ))]} == "$p1_symbol" && ${m[(( 2 + $y ))]} == "$p1_symbol" ]]; then
			echo 1
			exit 0
		fi
		if [[ ${m[(( $y ))]} == "$p2_symbol" && ${m[(( 1 + $y ))]} == "$p2_symbol" && ${m[(( 2 + $y ))]} == "$p2_symbol" ]]; then
			echo 2
			exit 0
		fi
	done
	for x in 0 1 2; do
		if [[ ${m[(( $x ))]} == "$p1_symbol" && ${m[(( $x + 3 ))]} == "$p1_symbol" && ${m[(( $x + 6 ))]} == "$p1_symbol" ]]; then
			echo 1
			exit 0
		fi
		if [[ ${m[(( $x ))]} == "$p2_symbol" && ${m[(( $x + 3 ))]} == "$p2_symbol" && ${m[(( $x + 6 ))]} == "$p2_symbol" ]]; then
			echo 2
			exit 0
		fi
	done
	if [[ ( ${m[0]} == "$p1_symbol" && ${m[4]} == "$p1_symbol" && ${m[8]} == "$p1_symbol" ) || ( ${m[2]} == "$p1_symbol" && ${m[4]} == "$p1_symbol" && ${m[6]} == "$p1_symbol" ) ]]; then
		echo 1
		exit 0
	fi
	if [[ ( ${m[0]} == "$p2_symbol" && ${m[4]} == "$p2_symbol" && ${m[8]} == "$p2_symbol" ) || ( ${m[2]} == "$p2_symbol" && ${m[4]} == "$p2_symbol" && ${m[6]} == "$p2_symbol" ) ]]; then
		echo 2
		exit 0
	fi
	if [[ ${m[0]} != " " && ${m[1]} != " " && ${m[2]} != " " && ${m[3]} != " " && ${m[4]} != " " && ${m[5]} != " " && ${m[6]} != " " && ${m[7]} != " " && ${m[8]} != " " ]]; then
		echo 3
		exit 0
	fi
	echo 0
	exit 0
}

function pvc() {
	local m=(" " " " " " " " " " " " " " " " " ");
	local display_mode=$1
	local p1_count=0
	local p2_count=0
	local error=0
	while true ; do
		if [[ $order == 0 ]]; then
			clear
			display "${m[@]}" "$display_mode"
			if [[ $error == 1 ]]; then
				echo "Incorrect input!"
				error=0
			fi
			read -p "Select column (1-3): " x
			read -p "Select row (1-3): " y
			if [[ ${m[(( $x - 1 + ( $y - 1 ) * 3 ))]} == " " && $x =~ ^[0-9]+$ && $y =~ ^[0-9]+$ ]]; then
				m[(( $x - 1 + ( $y - 1 ) * 3 ))]="$p1_symbol"
				p1_count=$(( p1_count + 1 ))
			else
				error=1
				continue
			fi
			score=$(check_board "${m[@]}")
			case $score in
			0) ;;
			1)
				clear
				display "${m[@]}" "$display_mode"
				printf "Player wins"
				if [[ $count == 1 ]]; then
					printf " after $p1_count moves!\nThe computer made $p2_count moves.\n"
				else
					printf "!\n"
				fi
				read wait
				return 0
			;;
			3)
				clear
				display "${m[@]}" "$display_mode"
				printf "It's a draw"
				if [[ $count == 1 ]]; then
					printf " after the Player made $p1_count moves!\nThe computer made $p2_count moves.\n"
				else
					printf "!\n"
				fi
				read wait
				return 0
			;;
			esac
		fi
		# Player submitted their move
		if [[ $error == 0 ]]; then
			clear
			display "${m[@]}" "$display_mode"
			x=$(npcx "${m[@]}")
			y=$(npcy "${m[@]}" "$x")
			for i in 0 1 2; do
				printf "\rThe computer is thinking   "
				sleep 0.2
				printf "\rThe computer is thinking.  "
				sleep 0.2
				printf "\rThe computer is thinking.. "
				sleep 0.2
				printf "\rThe computer is thinking..."
				sleep 0.2
			done
			m[(( $x - 1 + ( $y - 1 ) * 3 ))]="$p2_symbol"
			p2_count=$(( p2_count + 1 ))
			score=$(check_board "${m[@]}")
			case $score in
			0) ;;
			2)
				clear
				display "${m[@]}" "$display_mode"
				printf "Computer wins"
				if [[ $count == 1 ]]; then
					printf " after $p2_count moves!\nPlayer made $p1_count moves.\n"
				else
					printf "!\n"
				fi
				read wait
				return 0
			;;
			3)
				clear
				display "${m[@]}" "$display_mode"
				printf "It's a draw"
				if [[ $count == 1 ]]; then
					printf " after the Player made $p1_count moves!\nThe computer made $p2_count moves.\n"
				else
					printf "!\n"
				fi
				read wait
				return 0
			;;
			esac
		fi
		if [[ $order == 1 ]]; then
			clear
			display "${m[@]}" "$display_mode"
			if [[ $error == 1 ]]; then
				echo "Incorrect input!"
				error=0
			fi
			read -p "Select column (1-3): " x
			read -p "Select row (1-3): " y
			if [[ ${m[(( $x - 1 + ( $y - 1 ) * 3 ))]} == " " && $x =~ ^[0-9]+$ && $y =~ ^[0-9]+$ ]]; then
				m[(( $x - 1 + ( $y - 1 ) * 3 ))]="$p1_symbol"
				p1_count=$(( p1_count + 1 ))
			else
				error=1
				continue
			fi
			score=$(check_board "${m[@]}")
			case $score in
			0) ;;
			1)
				clear
				display "${m[@]}" "$display_mode"
				printf "Player wins"
				if [[ $count == 1 ]]; then
					printf " after $p1_count moves!\nThe computer made $p2_count moves.\n"
				else
					printf "!\n"
				fi
				read wait
				return 0
			;;
			3)
				clear
				display "${m[@]}" "$display_mode"
				printf "It's a draw"
				if [[ $count == 1 ]]; then
					printf " after the Player made $p1_count moves!\nThe computer made $p2_count moves.\n"
				else
					printf "!\n"
				fi
				read wait
				return 0
			;;
			esac
		fi
	done
}

function pvp() {
	local m=(" " " " " " " " " " " " " " " " " ");
	local display_mode=$1
	local player=1
	local p1_count=0
	local p2_count=0
	while true ; do
		clear
		display "${m[@]}" "$display_mode"
		if [[ $player == 1 ]]; then
			if [[ $error == 1 ]]; then
				echo "Incorrect input!"
				error=0
			fi
			echo "> Player One"
			read -p "Select column (1-3): " x
			read -p "Select row (1-3): " y
			if [[ ${m[(( $x - 1 + ( $y - 1 ) * 3 ))]} == " " && $x =~ ^[0-9]+$ && $y =~ ^[0-9]+$ ]]; then
				m[(( $x - 1 + ( $y - 1 ) * 3 ))]="$p1_symbol"
				p1_count=$(( p1_count + 1 ))
				player=2
			else
				error=1
				continue
			fi
			score=$(check_board "${m[@]}")
				case $score in
			0) ;;
			1)
				clear
				display "${m[@]}" "$display_mode"
				printf "Player One wins"
				if [[ $count == 1 ]]; then
					printf " after $p1_count turns!\nPlayer Two made $p2_count moves.\n"
				else
					printf "!\n"
				fi
				read wait
				return 0
			;;
			3)
				clear
				display "${m[@]}" "$display_mode"
				printf "It's a draw"
				if [[ $count == 1 ]]; then
					printf " after Player One made $p1_count moves!\nPlayer Two made $p2_count moves.\n"
				else
					printf "!\n"
				fi
				read wait
				return 0
			;;
			esac
		else
			if [[ $error == 1 ]]; then
				echo "Incorrect input!"
				error=0
			fi
			echo "> Player Two"
			read -p "Select column (1-3): " x
			read -p "Select row (1-3): " y
			if [[ ${m[(( $x - 1 + ( $y - 1 ) * 3 ))]} == " " && $x =~ ^[0-9]+$ && $y =~ ^[0-9]+$ ]]; then
				m[(( $x - 1 + ( $y - 1 ) * 3 ))]="$p2_symbol"
				p2_count=$(( p2_count + 1 ))
				player=1
			else
				error=1
				continue
			fi
			score=$(check_board "${m[@]}")
			case $score in
			0) ;;
			2)
				clear
				display "${m[@]}" "$display_mode"
				printf "Player Two wins"
				if [[ $count == 1 ]]; then
					printf " after $p2_count turns!\nPlayer One made $p1_count moves.\n"
				else
					printf "!\n"
				fi
				read wait
				return 0
			;;
			3)
				clear
				display "${m[@]}" "$display_mode"
				printf "It's a draw"
				if [[ $count == 1 ]]; then
					printf " after Player One made $p1_count moves!\nPlayer Two made $p2_count moves.\n"
				else
					printf "!\n"
				fi
				read wait
				return 0
			;;
			esac
		fi
	done
}

copyright
printf "\n"
while true; do
	echo "Bash-Tac-Toe"
	echo "1. Player vs Computer"
	echo "2. Player vs Player"
	echo "3. Settings"
	echo "4. Quit"
	read -p "> " selection
	case $selection in
	1|1.)
		pvc "$display_mode"
	;;
	2|2.)
		pvp "$display_mode"
	;;
	3|3.)
		while true; do
		clear
		echo "Settings"
		printf "1. Change display. Current - "
		if [[ $display_mode == 0 ]]; then
			printf "classic\n"
		else
			printf "modern\n"
		fi
		printf "2. Player One symbol. Current - $p1_symbol\n"
		printf "3. Player Two (NPC) symbol. Current - $p2_symbol\n"
		printf "4. Change play order. Current - "
		if [[ $order == 0 ]]; then
			printf "player first\n"
		else
			printf "computer first\n"
		fi
		printf "5. Toogle turn count. Current - "
		if [[ $count == 0 ]]; then
			printf "off\n"
		else
			printf "on\n"
		fi
		printf "6. Back\n"
		read -p "> " selection
		case $selection in
		1|1.)
			if [[ $display_mode == 0 ]]; then
				display_mode=1
			else
				display_mode=0
			fi
		;;
		2|2.)
			clear
			read -p "Provide the symbol for Player One: " p1_symbol
			p1_symbol=${p1_symbol::1}
		;;
		3|3.)
			clear
			read -p "Provide the symbol for Player One: " p2_symbol
			p2_symbol=${p2_symbol::1}
		;;
		4|4.)
			if [[ $order == 0 ]]; then
				order=1
			else
				order=0
			fi
		;;
		5|5.)
			if [[ $count == 0 ]]; then
				count=1
			else
				count=0
			fi
		;;
		6|6.)
			break
		;;
		esac
		done
	;;
	4|4.)
		exit 0
	;;
	esac
	clear
done
